# Musicmeet

## Server
The server of the Musicmeet APP is a Node.js server that handles user logins and serves
a RESTful API that manages events.

## Client
The client of the Musicmeet APP is an Angular webapp from which a user can create an
account, login, and start meeting up with new musicians!

## Installation and Setup
In order to get the entire application up and running, we need three components to
be running. 

### 1. Set up the database
In this application we are using MongoDB, so it is required to have it installed. Once
we have it installed locally or on a server, we run `mongod` in the terminal.

### 2. Run the API
The API connects to the MongoDB database and handles requests from the client. In order
to get this up and running, we need to install `yarn` locally. Then we can install
all the dependencies for the `express` API by running `yarn install` in the API directory.
Now we can finally fun the API by running the command `yarn dev`. This will get the API up
running in development mode.

### 3. Launch the Client
Now that we have the entire back-end set-up, we need a client application for the users
to use. This application will utilize the API to create, store and read data of users
as well as events.