/**
 * File: src/controllers/user-controller.ts
 * Created by: Sander Shi
 * Created date: 12/20/2018
 */

// Imports
import { Request, Response } from "express";
import * as mongoose from "mongoose";
import { UserSchema } from "../models/user";

// User
const User = mongoose.model("User", UserSchema);

export class UserController {

    public addNewUser(req: Request, res: Response) {
        const newUser = new User(req.body);

        newUser.save((err, user) => {
            if (err) {
                res.send(err);
            }
            res.json(user);
        });
    }

    public getProfile(req: Request, res: Response) {
        User.findOne({}, (err, user) => {
            if (err) {
                res.send(err);
            }
            res.json(user);
        });
    }
}
