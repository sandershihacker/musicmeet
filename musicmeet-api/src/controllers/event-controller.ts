/**
 * File: src/controllers/event-controller.ts
 * Created by: Sander Shi
 * Created date: 12/16/2018
 */

// Imports
import { Request, Response } from "express";
import * as mongoose from "mongoose";
import { EventSchema } from "../models/event";

// Event
const Event = mongoose.model("Event", EventSchema);

export class EventController {

    public addNewEvent(req: Request, res: Response) {
        const newEvent = new Event(req.body);
        newEvent.save((err, event) => {
            if (err) {
                res.send(err);
            }
            res.json(event);
        });
    }

    public getAllEvents(req: Request, res: Response) {
        Event.find({}, (err, event) => {
            if (err) {
                res.send(err);
            }
            res.json(event);
        });
    }
}