/**
 * File: src/routes/swagger.ts
 * Created by: Sander Shi
 * Created date: 12/16/2018
 */

// Imports
import { Application, Request, Response } from "express";
import * as swaggerUI from "swagger-ui-express";
import * as swaggerDocument from "../swagger.json";

// Class Definition
export class Swagger {

    public routes(app: Application): void { // Received the express instance from app.ts

        // Swagger Docs
        app.use("/", swaggerUI.serve, swaggerUI.setup(swaggerDocument));
    }
}
