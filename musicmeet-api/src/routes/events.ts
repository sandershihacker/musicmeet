/**
 * File: src/routes/events.ts
 * Created by: Sander Shi
 * Created date: 12/16/2018
 */

// Imports
import { Application, Request, Response } from "express";
import { EventController } from "../controllers/event-controller";

// Class Definition
export class Events {

    public eventController: EventController = new EventController();

    public routes(app: Application): void { // Received the express instance from app.ts
        app.route("/events").get(this.eventController.getAllEvents);
        app.route("/create").post(this.eventController.addNewEvent);
    }
}
