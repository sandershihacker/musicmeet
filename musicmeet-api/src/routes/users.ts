/**
 * File: src/routes/users.ts
 * Created by: Sander Shi
 * Created date: 11/08/2018
 */

// Imports
import { NextFunction } from "connect";
import { Application, Request, Response } from "express";
import * as passport from "passport";

// Class Definition
export class Users {

    public routes(app: Application): void { // Received the express instance from app.ts

        // Login
        app.route("/login").get((req: Request, res: Response, next: NextFunction) => {
            res.render("/users/login", {
                title: "Login",
            });
            next();
        });

        // Sign-up
        app.route("/signup").get((req: Request, res: Response, next) => {
            res.render("/users/register", {
                title: "Register",
            });
            next();
        });

        // Logout
        app.route("/logout").get((req: Request, res: Response) => {
            // TODO: Logout using passport
            res.status(200).send("Successfully Logged Out");
        });

        // Profile
        app.route("/profile").get((req: Request, res: Response) => {
            // TODO: Send profile data for user if authenticated
            res.status(200).send("User Authenticated");
        });
    }
}
