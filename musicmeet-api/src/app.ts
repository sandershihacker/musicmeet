/**
 * File: src/app.ts
 * Created by: Sander Shi
 * Created date: 11/08/2018
 */

// Imports
import * as bodyParser from "body-parser";
import * as express from "express";
import * as mongoose from "mongoose";

import { Events } from "./routes/events";
import { Swagger } from "./routes/swagger";
import { Users } from "./routes/users";

import { dbconfig } from "./config/database";

// Class definition
class App {

    public app: express.Application;
    public userRoutes: Users = new Users();
    public eventRoutes: Events = new Events();
    public swaggerRoutes: Swagger = new Swagger();

    constructor() {
        // Create App
        this.app = express();

        // Configure App
        this.config();

        // Set Up Routes
        this.userRoutes.routes(this.app);
        this.eventRoutes.routes(this.app);
        this.swaggerRoutes.routes(this.app);

        // Set Up Database
        this.mongoSetup();
    }

    private config(): void {
        // Support application/json type post data
        this.app.use(bodyParser.json());

        // Support application/x-www-form-urlencoded post data
        this.app.use(bodyParser.urlencoded({
            extended: false,
        }));
    }

    private mongoSetup(): void {
        (<any>mongoose).Promise = global.Promise;
        mongoose.connect(dbconfig.url, { useNewUrlParser: true });
    }
}

// Export App
export default new App().app;
