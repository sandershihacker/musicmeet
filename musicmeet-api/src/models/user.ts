/**
 * File: src/models/user.ts
 * Created By: Sander Shi
 * Created Date: 11/13/2018
 */

// Imports
import * as bcrypt from "bcrypt";
import { Schema } from "mongoose";

// User Schema
export const UserSchema: Schema = new Schema({
    createdAt: Date,
    password: {
        required: true,
        type: String,
    },
    username: {
        required: true,
        type: String,
        unique: true,
    },
});
UserSchema.pre("save", (next) => {
    const now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
    next();
});
UserSchema.methods.generateHash = (password: string) => {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8));
};
UserSchema.methods.validPassword = (password: string) => {
    return bcrypt.compareSync(password, this.password);
};

// export const createUser = (newUser, callback) => {
//     bcrypt.genSalt(10, (err, salt) => {
//         bcrypt.hash(newUser.password, salt, (err, hash) => {
//             newUser.password = hash;
//             newUser.save(callback);
//         });
//     });
// };
