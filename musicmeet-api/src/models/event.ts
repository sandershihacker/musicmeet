/**
 * File: src/models/event.ts
 * Created By: Sander Shi
 * Created Date: 12/16/2018
 */

// Imports
import { Schema } from "mongoose";

// User Schema
export const EventSchema: Schema = new Schema({
    date: String,
    description: String,
    limit: String,
    location: String,
    name: String,
});
EventSchema.pre("save", (next) => {
    if (this.limit === "10+") {
        this.limit = "None";
    }
    next();
});
