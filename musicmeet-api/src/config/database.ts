/**
 * File: src/config/database.ts
 * Created by: Sander Shi
 * Created date: 11/16/2018
 */

// Export
export const dbconfig = {
    url: "mongodb://localhost:27017/musicmeet",
};
