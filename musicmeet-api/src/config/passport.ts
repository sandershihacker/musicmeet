/**
 * File: src/config/passport.ts
 * Created By: Sander Shi
 * Created Date: 11/16/2018
 */

// Imports
import * as localStrategy from "passport-local";
const LocalStrategy = localStrategy.Strategy;
