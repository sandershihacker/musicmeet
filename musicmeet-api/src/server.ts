/**
 * File: src/server.ts
 * Created by: Sander Shi
 * Created date: 11/08/2018
 */

// Imports
import app from "./app";

// Constants
const PORT = process.env.PORT || 3000;

// Listen on port
app.listen(PORT, () => {
    console.log("Listening on port " + PORT);
});
