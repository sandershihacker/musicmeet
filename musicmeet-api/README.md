# Musicmeet API
---

This is the API for the Musicmeet app. The app will communicate with the API to perform user authentication, signup, login and logout. It will also fetch user data and event data so that this information can be displayed on the web application.

---
## Installation

To get the API up and running, we need to first install `yarn`. There is more information about the package manager [here](https://yarnpkg.com/lang/en/docs/install/#debian-stable). After that is installed, clone the repository and run `yarn install` in the directory of the repository. This will install all the packages needed to get the server up and running.

---
## Running the Server

The API server can be run in different modes. For development purposes, it is easier to run the command `yarn dev`. This will by default have the server up and running on `port 3000`. 

For production purposese, we can run the command `yarn start`.

---
## Endpoints


**/events (GET)**
This endpoint allows the user to fetch a list of all events that will be happening.

**/create (POST)**
This endpoint allows the user to create an event.

**/user/:username (GET)**
This endpoint fetches all profile data for a given user. It is used by only the user.

**/login**
This endpoint is just for login.

**/logout**
This endpoint logs the user out.

**/signup**
This endpoint helps the user sign up for the service.

---
## Credits
Designed and developed by [Sander Shi](http://www.sandershi.com).