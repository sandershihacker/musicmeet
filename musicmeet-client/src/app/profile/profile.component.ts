import { Component, OnInit } from '@angular/core';
import { MatChipInputEvent } from '@angular/material';
import { COMMA, ENTER } from '@angular/cdk/keycodes';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

    name = "";
    description = "";
    addOnBlur = true;
    readonly separatorKeysCodes: number[] = [ENTER, COMMA];
    instruments: string[] = [];

    constructor() { }

    ngOnInit() {
        this.name = "Sander Shi";
        this.description = "This is a short description.";
        this.instruments = ["Piano", "Guitar", "Tenor Saxophone", "Trumpet"];
    }

    add(event: MatChipInputEvent): void {
        const input = event.input;
        const value = event.value;

        // Add our fruit
        if ((value || '').trim()) {
            this.instruments.push(value.trim());
        }

        // Reset the input value
        if (input) {
            input.value = '';
        }
    }

    remove(instrument: string): void {
        const index = this.instruments.indexOf(instrument);

        if (index >= 0) {
            this.instruments.splice(index, 1);
        }
    }
}
