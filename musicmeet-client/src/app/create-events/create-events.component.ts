import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
	selector: 'app-create-events',
	templateUrl: './create-events.component.html',
	styleUrls: ['./create-events.component.scss']
})
export class CreateEventsComponent implements OnInit {

	editName = false;
	numbers = ["None", "1", "2", "3", "4", "5", "6 - 10", "10+"];

	newName = "New Jam Session";
	newDate = new Date();
	newLocation = "";
	newDescription = "";
	newMaxPeople = "None";

	constructor(private apiService: ApiService) { }

	ngOnInit() {
	}

	submit() {
		var newEvent = {
			name: this.newName,
			date: this.newDate.toDateString(),
			location: this.newLocation,
			description: this.newDescription,
			limit: this.newMaxPeople,
		}
		this.apiService.createEvent(newEvent).subscribe((data) => {
			console.log(data);
		});
	}

}
