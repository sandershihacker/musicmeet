import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    API_URL = "http://localhost:3000";

    constructor(private httpClient: HttpClient) { }

    getEvents() {
        return this.httpClient.get(`${this.API_URL}/events`);
    }

    createEvent(event) {
        return this.httpClient.post(`${this.API_URL}/create`, event);
    }

}
