import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Musicmeet';
  loggedIn = true;

  private login() {
    this.loggedIn = false;
  }

  private logout() {
    this.loggedIn = true;
  }

}
