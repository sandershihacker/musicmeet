# MusicmeetClient

This is the web client for the Musicmeet application. It communicates with the Musicmeet
API to perform queries and operations.

## Installation
### 1. Install Dependencies
Since this is an Angular application, we are using NPM as our package manager. Therefore,
to install our dependencies, we need to run `npm install` before using the application.

### 2. Install Angular-CLI
Before we can run the Angular web application, we need to install the Angular CLI.
This can be downloaded from the official website, or installed via a package
manager of your own choice, like `pacman` for Arch Linux, `apt` for Ubuntu etc.
After we have this installed, we can start using the angular-cli commands to
run, test and build the front-end application.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
